/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import skins4.entity.Grupo;
import skins4.entity.Grupoxusuario;
import skins4.entity.Usuario;

/**
 *
 * @author masterinftel21
 */
@Stateless
public class GrupoxusuarioFacade extends AbstractFacade<Grupoxusuario> {
    @PersistenceContext(unitName = "Skins4-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GrupoxusuarioFacade() {
        super(Grupoxusuario.class);
    }
    
    public synchronized  Grupoxusuario createAndReturn(Grupoxusuario gu){
         this.create(gu);
         return (Grupoxusuario) em.createQuery("SELECT gu FROM Grupoxusuario gu ORDER BY gu.idGrupoxusuario DESC").getResultList().get(0);
    }
    public List<Grupoxusuario> getAllGruposUsuario(Usuario usuario){
    
        return em.createNamedQuery("Grupoxusuario.findByIdUsuario").setParameter("idusuario", usuario.getIdUsuario()).getResultList();
    }
    
     public List<Grupoxusuario> getAllUsuariosGrupo(Grupo grupo){
        
       // List<Usuario> usuariosGrupo = new ArrayList<>();
        List<Grupoxusuario> lu = em.createNamedQuery("Grupoxusuario.findByIdGrupo").setParameter("idgrupo", grupo.getIdGrupo()).getResultList();
//         for (Grupoxusuario grupoxusuario : lu) {
//             usuariosGrupo.add(grupoxusuario.getIdusuario());
//         }
        return lu;
     }
     
     public List<Grupo> getAllGrupos(Usuario usuario){
        
        List<Grupo> grupos = new ArrayList<>();
        //List<Grupoxusuario> lu = em.createNamedQuery("Grupoxusuario.findByIdGrupo").setParameter("idgrupo", grupo.getIdGrupo()).getResultList();
         for (Grupoxusuario grupoxusuario : getAllGruposUsuario(usuario)) {
             grupos.add(grupoxusuario.getIdgrupo());
         }
        return grupos;
     }
}
