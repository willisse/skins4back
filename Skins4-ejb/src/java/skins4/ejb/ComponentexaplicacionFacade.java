/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import skins4.entity.Aplicacion;
import skins4.entity.Componente;
import skins4.entity.Componentexaplicacion;

/**
 *
 * @author masterinftel21
 */
@Stateless
public class ComponentexaplicacionFacade extends AbstractFacade<Componentexaplicacion> {
    @PersistenceContext(unitName = "Skins4-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ComponentexaplicacionFacade() {
        super(Componentexaplicacion.class);
    }
    
    public List<Componente> obtenerComponentes (String col, Aplicacion app){
        
        Query query1 = em.createQuery("SELECT c.idcomponente FROM Componentexaplicacion c WHERE (c.columna LIKE :value AND c.idaplicacion = :value2)").setParameter("value", col).setParameter("value2", app);
        
        List<Componente> comp;
        
        try{
            comp = (List<Componente>) query1.getResultList();
        }
        catch(NoResultException e){
            comp = null;
        }
        
        return comp;
    }
    
    public void eliminarComp(Aplicacion app){
        
        Query query1 = em.createQuery("SELECT c FROM Componentexaplicacion c WHERE c.idaplicacion = :value").setParameter("value", app);
        
        List<Componentexaplicacion> comp;
        
        try{
            comp = (List<Componentexaplicacion>) query1.getResultList();
        }
        catch(NoResultException e){
            comp = null;
        }
        
        if(comp!=null){
            for(Componentexaplicacion c: comp){
                remove(c);
            }
        }
    }
}
