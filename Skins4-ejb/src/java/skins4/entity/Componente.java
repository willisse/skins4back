/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author masterinftel21
 */
@Entity
@Table(name = "COMPONENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Componente.findAll", query = "SELECT c FROM Componente c"),
    @NamedQuery(name = "Componente.findByIdComponente", query = "SELECT c FROM Componente c WHERE c.idComponente = :idComponente"),
    @NamedQuery(name = "Componente.findByNombre", query = "SELECT c FROM Componente c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Componente.findByImagen", query = "SELECT c FROM Componente c WHERE c.imagen = :imagen")})
public class Componente implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcomponente")
    private List<Componentexaplicacion> componentexaplicacionList;
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_COMPONENTE")
    private BigDecimal idComponente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "IMAGEN")
    private String imagen;

    public Componente() {
    }

    public Componente(BigDecimal idComponente) {
        this.idComponente = idComponente;
    }

    public Componente(BigDecimal idComponente, String nombre, String imagen) {
        this.idComponente = idComponente;
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public BigDecimal getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(BigDecimal idComponente) {
        this.idComponente = idComponente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComponente != null ? idComponente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Componente)) {
            return false;
        }
        Componente other = (Componente) object;
        if ((this.idComponente == null && other.idComponente != null) || (this.idComponente != null && !this.idComponente.equals(other.idComponente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "skins4.ejb.Componente[ idComponente=" + idComponente + " ]";
    }

    @XmlTransient
    public List<Componentexaplicacion> getComponentexaplicacionList() {
        return componentexaplicacionList;
    }

    public void setComponentexaplicacionList(List<Componentexaplicacion> componentexaplicacionList) {
        this.componentexaplicacionList = componentexaplicacionList;
    }
    
}
