/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.bean;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import skins4.ejb.AplicacionFacade;
import skins4.ejb.GrupoFacade;
import skins4.ejb.GrupoxusuarioFacade;
import skins4.ejb.UsuarioFacade;
import skins4.entity.Aplicacion;
import skins4.entity.Grupo;
import skins4.entity.Grupoxusuario;
import skins4.entity.Usuario;


/**
 *
 * @author Driss
 */
@ManagedBean
@ViewScoped
public class GrupoBean implements Serializable {
        @EJB
        private UsuarioFacade usuarioFacade;
        @EJB
        private GrupoFacade grupoFacade;
     
        @EJB
        private GrupoxusuarioFacade grupoxusuarioFacade;
        
        @EJB
        private AplicacionFacade aplicacionFacade;


    /**
     * Creates a new instance of GrupoBean
     */
    
    ExternalContext contexto;
    LoginBean referenciaLogin;
    List<Grupoxusuario> gruposUsuario;
    List<Usuario> usuariosGrupo;
    List<Grupoxusuario> usuariosGrupoSeleccionado;
    private List<Aplicacion> aplicacionesGrupoSeleccionado;
    Usuario usuarioLogueado;
    Grupo grupoSeleccionado;
    int numUsuariosGrupo;
    String loginUsuarioGrupo;
    String error, errorGrupo;
    Grupoxusuario grupoxuserToLeave;
     private List<Grupo> gruposTamano3;
    Grupoxusuario usuarioAEliminar;

    public List<Aplicacion> getAplicacionesGrupoSeleccionado() {
        return aplicacionesGrupoSeleccionado;
    }

    public void setAplicacionesGrupoSeleccionado(List<Aplicacion> aplicacionesGrupoSeleccionado) {
        this.aplicacionesGrupoSeleccionado = aplicacionesGrupoSeleccionado;
    }

    public String getErrorGrupo() {
        return errorGrupo;
    }

    public void setErrorGrupo(String errorGrupo) {
        this.errorGrupo = errorGrupo;
    }
   


   
   
   
    public Grupoxusuario getUsuarioAEliminar() {
        return usuarioAEliminar;
    }

    public void setUsuarioAEliminar(Grupoxusuario usuarioAEliminar) {
        this.usuarioAEliminar = usuarioAEliminar;
    }
    
    
    public List<Grupo> getGruposTamano3() {
        return gruposTamano3;
    }

    public void setGruposTamano3(List<Grupo> gruposTamano3) {
        this.gruposTamano3 = gruposTamano3;
    }
     
     
     
    public Grupoxusuario getGrupoxuserToLeave() {
        return grupoxuserToLeave;
    }

    public void setGrupoxuserToLeave(Grupoxusuario grupoxuserToLeave) {
        this.grupoxuserToLeave = grupoxuserToLeave;
    }
    
    
    public List<Grupoxusuario> getUsuariosGrupoSeleccionado() {
        return usuariosGrupoSeleccionado;
    }

    public void setUsuariosGrupoSeleccionado(List<Grupoxusuario> usuariosGrupoSeleccionado) {
        this.usuariosGrupoSeleccionado = usuariosGrupoSeleccionado;
    }
    
    
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    
    public String getLoginUsuarioGrupo() {
        return loginUsuarioGrupo;
    }

    public void setLoginUsuarioGrupo(String loginUsuarioGrupo) {
        this.loginUsuarioGrupo = loginUsuarioGrupo;
    }

    
    public void setNumUsuariosGrupo(int numUsuariosGrupo) {
        this.numUsuariosGrupo = numUsuariosGrupo;
    }

    public Grupo getGrupoSeleccionado() {
        return grupoSeleccionado;
    }

    public void setGrupoSeleccionado(Grupo grupoSeleccionado) {
        this.grupoSeleccionado = grupoSeleccionado;
    }
    
    public GrupoBean() {
    }

   
   
  
//    public List<Usuario> getUsuariosGrupo() {
//        usuariosGrupo = new ArrayList<>();
//        for (Grupoxusuario grupoxusuario : grupoSeleccionado.getGrupoxusuarioList()) {
//            usuariosGrupo.add(grupoxusuario.getIdusuario());
//        }
//        return usuariosGrupo;
//    }

    public void setUsuariosGrupo(List<Usuario> usuariosGrupo) {
        this.usuariosGrupo = usuariosGrupo;
    }

   


    public List<Grupoxusuario> getGruposUsuario() {
        return gruposUsuario;
    }

    public void setGruposUsuario(List<Grupoxusuario> gruposUsuario) {
        this.gruposUsuario = gruposUsuario;
    }

    public Usuario getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(Usuario usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }

   
    
    @PostConstruct
    void cargar(){
        contexto = FacesContext.getCurrentInstance().getExternalContext();
        referenciaLogin = (LoginBean) contexto.getSessionMap().get("loginBean");
        usuarioLogueado = referenciaLogin.getUser();
        gruposUsuario = grupoxusuarioFacade.getAllGruposUsuario(usuarioLogueado);  
        grupoSeleccionado=referenciaLogin.getGrupoSeleccionado();
        if(grupoSeleccionado!=null){
            setUsuariosGrupoSeleccionado(grupoxusuarioFacade.getAllUsuariosGrupo(grupoSeleccionado));
             setAplicacionesGrupoSeleccionado(aplicacionFacade.findAplicacionesGrupo(grupoSeleccionado));
        }
        
        
       // gruposTamano3 = inicializarGrupos();
    }
    
    void sincronizar(){
        
        setGruposUsuario(grupoxusuarioFacade.getAllGruposUsuario(usuarioLogueado));
      //  referenciaLogin.setGruposUsuario(grupoxusuarioFacade.getAllGrupos(usuarioLogueado));
        
      //  setGruposTamano3(inicializarGrupos());
       // referenciaLogin.setGruposTamano3(gruposTamano3);
    }
    
     public List<Grupo> inicializarGrupos() {
        List<Grupo> l = new ArrayList<>();
       
        
 
        if(getGruposUsuario().size() < 3){
            
          for (Grupoxusuario g : getGruposUsuario()){
                l.add(g.getIdgrupo());
          }  
        }else{
        for (Grupoxusuario g : getGruposUsuario().subList(getGruposUsuario().size()- 3, getGruposUsuario().size())){
                l.add(g.getIdgrupo());
          }  
        }
        return l;
      }
    
    private String nombreGrupo;

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }
    
    public String verGrupo(){
        setUsuariosGrupoSeleccionado(grupoxusuarioFacade.getAllUsuariosGrupo(grupoSeleccionado));
        setAplicacionesGrupoSeleccionado(aplicacionFacade.findAplicacionesGrupo(grupoSeleccionado));
        
    return "";
    }
    
    public String crearGrupo() {
        System.out.println("creo un grupo con su grupoXlogueado ");

        setErrorGrupo("");
        if (getNombreGrupo().isEmpty()) {
            setErrorGrupo("Debe introducir un nombre de grupo.");
        } else {
            if (perteneceAGruposUsuario(getNombreGrupo())) {
                setErrorGrupo("Este nombre de grupo ya existe.");
                setNombreGrupo("");
            } else {
                Grupo nuevo = new Grupo(BigDecimal.ZERO);
                nuevo.setNombre(nombreGrupo);
                nuevo.setPropietario(usuarioLogueado);
                grupoFacade.create(nuevo);
                referenciaLogin.actualizaGrupos();
                sincronizar();
                setNombreGrupo("");
                setErrorGrupo("");
            }
        }
        return "";
    }


   public String addUsuarioAGrupo(){
       System.out.println("Añado un grupoXusuario con GruposeleccXloginAñadir");
    setError("");
    if(getLoginUsuarioGrupo().isEmpty()){
            setError("Debe introducir un E-mail");
       } else{
           Usuario usu = usuarioFacade.findByEmail(loginUsuarioGrupo);
           usuariosGrupoSeleccionado = grupoxusuarioFacade.getAllUsuariosGrupo(grupoSeleccionado);
           if(null==usu){
            setError("El usuario no existe");
           }else if(usuariosGrupoSeleccionado.contains(usu)){
            setError("El usuario ya es participante en el grupo");
           }else{
              Grupoxusuario gu = new Grupoxusuario(BigDecimal.ZERO);
              gu.setIdgrupo(grupoSeleccionado);
              gu.setIdusuario(usu);
              
              grupoxusuarioFacade.createAndReturn(gu);
              usuariosGrupoSeleccionado = grupoxusuarioFacade.getAllUsuariosGrupo(grupoSeleccionado);
             // usu.getGrupoxusuarioList().add(grupoxusuarioFacade.createAndReturn(gu));
              // grupoSeleccionado.getGrupoxusuarioList().add(gu);
               setError("");
           }
      
       }
        setLoginUsuarioGrupo("");
        return "";
   }
   
   public String borrarGrupo () {
    grupoFacade.remove(grupoSeleccionado);
    referenciaLogin.actualizaGrupos();
    sincronizar();
     setGrupoSeleccionado(null);
//       grupoxusuarioFacade.remove(gxu);
//       referenciaLogin.getUser().getGrupoxusuarioList().remove(gxu);
       System.err.println("BORRO un grupo mio asi que borro sus GruposXusuarioLogueado");
       return "";
   }
   
   public String leaveGrupo () {
       grupoxusuarioFacade.remove(grupoxuserToLeave);
        referenciaLogin.actualizaGrupos();
       sincronizar();
       setGrupoSeleccionado(null);
       System.out.println("Abandono el grupo borro GrupoSeleccionadoXusuarioLogueado");
       return "";
   }
   
 
   
  public String borrarUsuarioGrupo(){
      
        
        grupoxusuarioFacade.remove(usuarioAEliminar);
        usuariosGrupoSeleccionado = grupoxusuarioFacade.getAllUsuariosGrupo(grupoSeleccionado);
        sincronizar();
        setGrupoSeleccionado(null);
        System.out.println("Abandono el grupo borro GrupoSeleccionadoXusuarioLogueado");
        return "";
  
  }

    private boolean perteneceAGruposUsuario(String nombreGrupo) {
       if(getGruposUsuario().isEmpty()){
           return false;
       } else {
           boolean pertenece = false;
           int i =0;
           while (!pertenece && i<getGruposUsuario().size()) {               
               if(getGruposUsuario().get(i).getIdgrupo().getNombre().equalsIgnoreCase(nombreGrupo)){
                    pertenece=true;
               }
               i++;
           }
           return pertenece;
       }
    }
}
