/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skins4.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import skins4.ejb.AplicacionFacade;
import skins4.ejb.ComponentexaplicacionFacade;
import skins4.ejb.GrupoFacade;

import skins4.ejb.UsuarioFacade;
import skins4.entity.Aplicacion;

/**
 *
 * @author masterinftel22
 */
@ManagedBean
@ViewScoped
public class AplicacionBean implements Serializable{
    
    @EJB
    private GrupoFacade grupoFacade;
    @EJB
    private AplicacionFacade aplicacionFacade;
    @EJB
    private UsuarioFacade usuarioFacade;
    @EJB
    private ComponentexaplicacionFacade componentexaplicacionFacade;
    
    LoginBean loginBean;
    GrupoBean grupoBean;
    
    private String nombre;
    private String descripcion;
    private String propietario;
    private BigDecimal grupo;
    Aplicacion aplicacionseleccionada;
    private List<Aplicacion> aplicacionesUsuario;

    public List<Aplicacion> getAplicacionesUsuario() {
        return aplicacionesUsuario;
    }

    public void setAplicacionesUsuario(List<Aplicacion> aplicacionesUsuario) {
        this.aplicacionesUsuario = aplicacionesUsuario;
    }
    
    @PostConstruct
    void cargar(){
        loginBean = ((LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean"));
        aplicacionesUsuario = aplicacionFacade.findAppByUsuario(loginBean.getUser());
        
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public BigDecimal getGrupo() {
        return grupo;
    }

    public void setGrupo(BigDecimal grupo) {
        this.grupo = grupo;
    }

    public GrupoFacade getGrupoFacade() {
        return grupoFacade;
    }

    public void setGrupoFacade(GrupoFacade grupoFacade) {
        this.grupoFacade = grupoFacade;
    }

    public AplicacionFacade getAplicacionFacade() {
        return aplicacionFacade;
    }

    public void setAplicacionFacade(AplicacionFacade aplicacionFacade) {
        this.aplicacionFacade = aplicacionFacade;
    }

    public UsuarioFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    public void setUsuarioFacade(UsuarioFacade usuarioFacade) {
        this.usuarioFacade = usuarioFacade;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Aplicacion getAplicacionseleccionada() {
        return aplicacionseleccionada;
    }

    public void setAplicacionseleccionada(Aplicacion aplicacionseleccionada) {
        this.aplicacionseleccionada = aplicacionseleccionada;
    }
    
    public AplicacionBean() {
        
    }
    
    public String crearAplicacion() {
        
       // loginBean = ((LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean"));
        propietario = loginBean.getLogin();
        
        Aplicacion a = new Aplicacion(BigDecimal.ZERO);
        a.setNombre(this.nombre);
        a.setDescripcion(this.descripcion);
        a.setFecha(new Date());
        a.setPropietario(usuarioFacade.findByEmail(propietario));
        
        if (this.grupo != null)
            a.setGrupo(grupoFacade.find(this.grupo));
        
        
        aplicacionFacade.create(a);
        aplicacionFacade.flush();
        setAplicacionesUsuario(aplicacionFacade.findAppByUsuario(loginBean.getUser()));
       
        //loginBean.setAplicacionesUsuario(aplicacionesUsuario);
        loginBean.actualizaAplicaciones();
        setGrupo(null);
        setNombre("");
        setDescripcion("");
        
        setAplicacionseleccionada(a);
        //editorBean.cargarItems();
        //loginBean.inicializarAplicaciones();
        return"editor";
        
    }
    
    public String modApp () {
        
        //El usuario existe y modificamos sus datos
        aplicacionseleccionada.setNombre(this.aplicacionseleccionada.getNombre());
        aplicacionseleccionada.setDescripcion(this.aplicacionseleccionada.getDescripcion());
        if (this.aplicacionseleccionada.getGrupo() != null)
            aplicacionseleccionada.setGrupo(grupoFacade.find(this.aplicacionseleccionada.getGrupo()));
        aplicacionFacade.edit(this.aplicacionseleccionada);
        
        return "";
    }
    
    public String editarInterfaz(){
        
        return "editor";
   }
      
   public String borrarAplicacion () {
       
      // loginBean = ((LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean"));
       componentexaplicacionFacade.eliminarComp(aplicacionseleccionada);
       aplicacionFacade.remove(aplicacionseleccionada);
       setAplicacionesUsuario(aplicacionFacade.findAppByUsuario(loginBean.getUser()));
       loginBean.actualizaAplicaciones();
       //loginBean.inicializarAplicaciones();
       setAplicacionseleccionada(null);
       return "";
   }
}
