/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package skins4.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;
import skins4.ejb.AplicacionFacade;

import skins4.ejb.GrupoxusuarioFacade;
import skins4.ejb.UsuarioFacade;
import skins4.entity.Aplicacion;
import skins4.entity.Grupo;
import skins4.entity.Usuario;

/**
 *
 * @author masterinftel21
 */
@ManagedBean
@SessionScoped
public class LoginBean implements Serializable{
    @EJB
    private AplicacionFacade aplicacionFacade;
    @EJB
    private GrupoxusuarioFacade grupoxusuarioFacade;
    
    @EJB
    private UsuarioFacade usuarioFacade;
    
    
    @Email
    private String login;
    
    @NotNull
    private String password1;
    private String password2;
    private String nombre;
    private String apellidos;
    private String telefono;
    private String error;
    
    private Usuario user;
    private List<Grupo> gruposTamano3;
    
    private List<Grupo> grupos;
    private Grupo grupoSeleccionado;
    private List<Aplicacion> aplicacionesUsuario;
    private List<Aplicacion> aplicacionesTamano3;
    
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public List<Aplicacion> getAplicacionesUsuario() {
        return aplicacionesUsuario;
    }

    public void setAplicacionesUsuario(List<Aplicacion> aplicacionesUsuario) {
        this.aplicacionesUsuario = aplicacionesUsuario;
    }

    public Grupo getGrupoSeleccionado() {
        return grupoSeleccionado;
    }

    public void setGrupoSeleccionado(Grupo grupoSeleccionado) {
        this.grupoSeleccionado = grupoSeleccionado;
    }
    
    
    
    public String getLogin() {
        return login;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }
    
    public String getPassword1() {
        return password1;
    }
    
    public void setPassword1(String password1) {
        this.password1 = password1;
    }
    
    public String getPassword2() {
        return password2;
    }
    
    public void setPassword2(String password2) {
        this.password2 = password2;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getApellidos() {
        return apellidos;
    }
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    
    public String getTelefono() {
        return telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public Usuario getUser() {
        return user;
    }
    
    public void setUser(Usuario user) {
        this.user = user;
    }
    
    public String getError() {
        return error;
    }
    
    public void setError(String error) {
        this.error = error;
    }
    
    public List<Grupo> getGrupos() {
        return grupos;
    }
    
    public void setGrupos(List<Grupo> grupos) {
        this.grupos = grupos;
    }
    
    public List<Grupo> getGruposTamano3() {
        return gruposTamano3;
    }
    
    public void setGruposTamano3(List<Grupo> gruposTamano3) {
        this.gruposTamano3 = gruposTamano3;
    }
    
    public List<Aplicacion> getAplicacionesTamano3() {
        return aplicacionesTamano3;
    }
    
    public void setAplicacionesTamano3(List<Aplicacion> aplicacionesTamano3) {
        this.aplicacionesTamano3 = aplicacionesTamano3;
    }
    
    public void actualizarGrupos() {
        this.grupos = user.getGrupoList();
    }
    
    public String logIn ()
    {
        setUser(usuarioFacade.findByEmail(this.login));
        
        if(this.getUser() != null)
        {
            //El usuario existe
            if(user.getPassword().equals(this.password1))
            {
                //La contraseña es correcta
                
                 actualizaGrupos();
                 actualizaAplicaciones();


                return "index";
            }
            else{
                setError("Contraseña incorrecta.");
                return "login";
            }
        }
        else
        {
            setError("El usuario no existe");
            setUser(null);
            return "login";
        }
    }
    
    public String signIn ()
    {
        Usuario usuario = usuarioFacade.findByEmail(this.login);
        
        if(usuario == null){
            
            //Usuario no existe
            Usuario nuevoUsuario = new Usuario();
            //nuevoUsuario.setId(usuarioFacade.getMaxID()+1)
            nuevoUsuario.setNombre(this.nombre);
            nuevoUsuario.setApellidos(this.apellidos);
            nuevoUsuario.setPassword(this.password1);
            nuevoUsuario.setLogin(this.login);
            nuevoUsuario.setTelefono(this.telefono);
            usuarioFacade.create(nuevoUsuario);
            return "index";
        }
        else
        {
            //Usuario existe
            return "login";
        }
    }
    
    public String logOut ()
    {
        setUser(null);
        
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/login.xhtml?faces-redirect=true";
    }
    
    public String modProfile () {
        
        //Usuario usuario = usuarioFacade.findByEmail(this.login);
        
        //El usuario existe y modificamos sus datos
        user.setNombre(this.user.getNombre());
        user.setApellidos(this.user.getApellidos());
        user.setTelefono(this.user.getTelefono());
        System.out.println("NOMBRE:" + this.user.getNombre());
        usuarioFacade.edit(this.user);
        
        return "confirmarEdit";
    }
    
    

    public void inicializarAplicaciones() {
        if (this.user.getAplicacionList() != null && this.user.getAplicacionList().size() > 0){
            if (this.user.getAplicacionList().size() < 3){
                aplicacionesTamano3 = new LinkedList<>(this.user.getAplicacionList().subList(0, this.user.getAplicacionList().size()));
            }
            else{
                aplicacionesTamano3 = new LinkedList<>(this.user.getAplicacionList().subList(0, 3));
            }
        }
        else{
            this.user.setAplicacionList(new LinkedList<Aplicacion>());
        }
    }

    public void actualizaGrupos() {
        grupos = grupoxusuarioFacade.getAllGrupos(user);
        if (!grupos.isEmpty()) {
            if (grupos.size() < 3) {
                gruposTamano3 = grupos.subList(0, grupos.size());
            } else {
                gruposTamano3 = grupos.subList(0, 3);
            }
        }else{
            setGruposTamano3(new ArrayList<Grupo>());
        }
    }
    
     public void actualizaAplicaciones() {
        aplicacionesUsuario = aplicacionFacade.findAppByUsuario(user);
        if (!aplicacionesUsuario.isEmpty()) {
            if (aplicacionesUsuario.size() < 3) {
                aplicacionesTamano3 = aplicacionesUsuario.subList(0, aplicacionesUsuario.size());
            } else {
                aplicacionesTamano3 = aplicacionesUsuario.subList(0, 3);
            }
        }else{
            setAplicacionesTamano3(new ArrayList<Aplicacion>());
        }
    }
    
}
